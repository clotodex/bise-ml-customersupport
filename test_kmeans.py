import json
from time import time

import numpy as np
from sklearn.cluster import KMeans
# import all metrics
from sklearn.metrics import davies_bouldin_score, silhouette_score, calinski_harabasz_score

import datamanager
from jqmcvi import dunn_fast

if __name__ == "__main__":
    # load the configuration from the config file
    with open("train_config.json") as conf:
        config = json.load(conf)
    print("config: ", config)

    # get training and testing data
    train_data, test_data, _ = datamanager.get_train_test_validation_data(config['data'], config['embedded_data'])

    for k in [5, 10, 20, 50]:
        print("#########")
        print("# k = {} #".format(k))
        print("#########")

        ## load kmeans model
        ## train kmeans model if not already trained ##
        print("fitting kmeans...")
        _start = time()
        km = KMeans(
            n_clusters=k, init='random',
            n_init=10, max_iter=300,
            tol=1e-04, random_state=0
        )
        km.fit(train_data)
        _end = time()
        print(f"done (took {_end - _start})")

        # predict on test data
        _start = time()
        y_pred = km.predict(test_data)
        _end = time()
        print(f"predicting took {_end - _start}")
        x = test_data

        # count of unique clusters
        uniq, count = np.unique(y_pred, return_counts=True)
        unique_count = len(uniq)
        print("metrics/uniqcount:", unique_count)
        # count per cluster
        # { i: (0 if i not in uniq else count[uniq.index(i)]) for i in range(n_clusters)}
        print(f"metrics/counthist: {uniq} - {count}")

        if unique_count > 1:
            metric_db = davies_bouldin_score(x, y_pred)
            print("metrics/db:", metric_db)
            metric_dunn = dunn_fast(x, y_pred)
            print("metrics/dunn:", metric_dunn)
            metric_shilouette = silhouette_score(x, y_pred, metric='euclidean', sample_size=None)
            print("metrics/shilouette:", metric_shilouette)
            metric_chs = calinski_harabasz_score(x, y_pred)
            print("metrics/chs:", metric_chs)
