if __name__ == "__main__":
    import re

    re_mention = re.compile(r'@[^\s]*')
    re_hashtag = re.compile(r'#[^\s]*')
    line = "@amazon @kathi helloooo #bla #blub"
    matches = re_mention.findall(line)
    matches += re_hashtag.findall(line)
    for m in matches:
        line = line.replace(m, '')
    line = line.strip()
    print(matches)
    print(line)
