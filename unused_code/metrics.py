# TODO: check which distance measure IDEC uses

import math

# several
import numpy as np
# Calinski-Harabasz Index & silhouette_2
from sklearn import metrics
# db and silhouette index
from sklearn.metrics import davies_bouldin_score
from sklearn.metrics import silhouette_score  # only necessary for silhouette_1

# dunn index
import jqmcvi


# x dunn optimieren - je größer desto besser - = db + je kleiner die cluster desto besser
# x db - kleiner ist besser - prüft dichte der cluster und entferung zu anderen
# x CHS - maximieren - var(abstand punkt zu center) / var(abstand cluster zu daten zentrum)
# SDBW - minimiert
# neuer silhouette - wertebereich -1 (sollte woanders hin), 0 (genau am wendepunkt) bis 1 (well assigned) - pro punkt score - 

############################################################
## THIS FILE SERVED AS A DUMP FOR ALL METRIC RELATED CODE ##
############################################################

def dunn_1(y_pred):
    return (jqmcvi.dunn(y_pred))


def db(X, y_pred):
    return (davies_bouldin_score(X, y_pred))


def silhouette_1(X, y_pred):
    return (silhouette_score(X, y_pred, metric='euclidean', sample_size=None))


def silhouette_2(X, y_pred):
    return (metrics.silhouette_score(X, y_pred, metric='euclidean'))


def chs(X, y_pred):
    return (metrics.calinski_harabasz_score(X, y_pred))


def dunn_2(c, distances):
    """
    Dunn index for cluster validation (the bigger, the better)

    .. math:: D = \\min_{i = 1 \\ldots n_c; j = i + 1\ldots n_c} \\left\\lbrace \\frac{d \\left( c_i,c_j \\right)}{\\max_{k = 1 \\ldots n_c} \\left(diam \\left(c_k \\right) \\right)} \\right\\rbrace

    where :math:`d(c_i,c_j)` represents the distance between
    clusters :math:`c_i` and :math:`c_j`, given by the distances between its
    two closest data points, and :math:`diam(c_k)` is the diameter of cluster
    :math:`c_k`, given by the distance between its two farthest data points.

    The bigger the value of the resulting Dunn index, the better the clustering
    result is considered, since higher values indicate that clusters are
    compact (small :math:`diam(c_k)`) and far apart.

    .. [Kovacs2005] Kovács, F., Legány, C., & Babos, A. (2005). Cluster validity measurement techniques. 6th International Symposium of Hungarian Researchers on Computational Intelligence.
    """
    unique_cluster_distances = np.unique(min_cluster_distances(c, distances))
    max_diameter = max(diameter(c, distances))

    if np.size(unique_cluster_distances) > 1:
        return unique_cluster_distances[1] / max_diameter
    else:
        return unique_cluster_distances[0] / max_diameter


def min_cluster_distances(c, distances):
    """Calculates the distances between the two nearest points of each cluster"""
    min_distances = np.zeros((max(c) + 1, max(c) + 1))
    for i in np.arange(0, len(c)):
        if c[i] == -1: continue
        for ii in np.arange(i + 1, len(c)):
            if c[ii] == -1: continue
            if c[i] != c[ii] and distances[i, ii] > min_distances[c[i], c[ii]]:
                min_distances[c[i], c[ii]] = min_distances[c[ii], c[i]] = distances[i, ii]
    return min_distances


def diameter(c, distances):
    """Calculates cluster diameters (the distance between the two farthest data points in a cluster)"""
    diameters = np.zeros(max(c) + 1)
    for i in np.arange(0, len(c)):
        if c[i] == -1: continue
        for ii in np.arange(i + 1, len(c)):
            if c[ii] == -1: continue
            if c[i] != -1 or c[ii] != -1 and c[i] == c[ii] and distances[i, ii] > diameters[c[i]]:
                diameters[c[i]] = distances[i, ii]
    return diameters


# adjust: cluster centers might be wrong | length density list dynamic?
class S_Dbw():
    def __init__(self, data, data_cluster, centers_index):
        """
        data --> raw data
        data_cluster --> The category to which each point belongs(category starts at 0)
        centers_index --> Cluster center index
        """
        self.data = data
        self.data_cluster = data_cluster
        self.centers_index = centers_index

        self.k = len(centers_index)

        # Initialize stdev
        self.stdev = 0
        for i in range(self.k):
            std_matrix_i = np.std(data[self.data_cluster == i], axis=0)
            self.stdev += math.sqrt(np.dot(std_matrix_i.T, std_matrix_i))
        self.stdev = math.sqrt(self.stdev) / self.k

    # written for k midioids - needs to be adjusted
    def density(self, density_list=[]):
        """
        compute the density of one or two cluster(depend on density_list)
        """
        density = 0
        centers_index1 = self.centers_index[density_list[0]]
        if len(density_list) == 2:
            centers_index2 = self.centers_index[density_list[1]]
            center_v = (self.data[centers_index1] + self.data[centers_index2]) / 2
        else:
            center_v = self.data[centers_index1]
        for i in density_list:
            temp = self.data[self.data_cluster == i]
            for j in temp:
                if np.linalg.norm(j - center_v) <= self.stdev:
                    density += 1
        return density

    def Dens_bw(self):
        density_list = []
        result = 0
        for i in range(self.k):
            density_list.append(self.density(density_list=[i]))
        for i in range(self.k):
            for j in range(self.k):
                if i == j:
                    continue
                result += self.density([i, j]) / max(density_list[i], density_list[j])
        return result / (self.k * (self.k - 1))

    def Scat(self):
        theta_s = np.std(self.data, axis=0)
        theta_s_2norm = math.sqrt(np.dot(theta_s.T, theta_s))
        sum_theta_2norm = 0

        for i in range(self.k):
            matrix_data_i = self.data[self.data_cluster == i]
            theta_i = np.std(matrix_data_i, axis=0)
            sum_theta_2norm += math.sqrt(np.dot(theta_i.T, theta_i))
        return sum_theta_2norm / (theta_s_2norm * self.k)

    def S_Dbw_result(self):
        """
        compute the final result
        """

    return self.Dens_bw() + self.Scat()


def computeS_Dbw(X, y_pred, cluster_centers):
    return (S_Dbw(X, y_pred, cluster_centers))
