import numpy as np

# library to make artificial cluster data
from sklearn.datasets import make_blobs


def cluster_kmeans(data, number_cluster):
    # import KMeans as predefined class
    from sklearn.cluster import KMeans

    # initialize KMeans with externally proven useful parameters.
    km = KMeans(
        n_clusters=number_cluster, init='random',
        n_init=10, max_iter=300,
        tol=1e-04, random_state=0
    )

    # run predefined method fit_predict from KMeans on dataset "data"
    data_km = km.fit_predict(data)

    # cluster_centers_ is parameter of class KMeans
    return data_km, km.cluster_centers_


def pcaplot(data, cluster, centers):
    import matplotlib.pyplot as plt  # library for plotting
    # noinspection PyUnresolvedReferences
    from mpl_toolkits.mplot3d import Axes3D  # enable matplotlib for 3D plots
    import pandas as pd  # library for data processing
    from sklearn.decomposition import PCA  # library for pca, PCA is a class

    # norm data for feature reduction
    data_norm = (data - data.min()) / (data.max() - data.min())

    # initialize class PCA with 3 components (equals 3 dimensions)
    pca = PCA(n_components=3)

    # perform 3-dimensional PCA from library sklearn.decomposition with method fit_transform (fit and transform at the same time)
    transformed_data = pd.DataFrame(pca.fit_transform(data_norm))

    # extract and transform center coordinates from object pca
    transformed_centers = pca.transform(centers)

    # initialize plot class from method figure (method contained in library plt)
    fig = plt.figure()

    # fig should only be one plot -> we choose the plot at "coordinates" 111
    ax = fig.add_subplot(111, projection="3d")

    # define color vector for plot visualization
    colors = ["red", "green", "blue", "yellow", "black", "pink", "orange", "purple"]

    # for each cluster: extract all datapoints and 3D-plot them (by method ax.scatter)
    for cluster_i in np.unique(cluster):
        print("painting cluster_i: ", cluster_i)
        ax.scatter(transformed_data[cluster == cluster_i][0],
                   transformed_data[cluster == cluster_i][1],
                   transformed_data[cluster == cluster_i][2],
                   label='Class {}'.format(cluster_i),
                   c=colors[cluster_i % len(colors)]
                   )

    # extract all centers and 3D-plot them
    for c in transformed_centers:
        ax.scatter(c[0], c[1], c[2], marker="*")

    # show legend and plot
    plt.legend()
    plt.show()


def create_test_data(n_samples, n_features, centers, cluster_std):
    data, y = make_blobs(
        n_samples=n_samples,
        n_features=n_features,
        centers=centers,
        cluster_std=cluster_std,
        shuffle=True,
        random_state=0
    )
    return data, y


# create point cloud with "labels" (not used yet)

def tensorboard(data, number_cluster):
    import os
    import tensorflow as tf
    from tensorflow.examples.tutorials.mnist import input_data
    from tensorflow.contrib.tensorboard.plugins import projector

    LOG_DIR = 'logs'
    metadata = os.path.join(LOG_DIR, 'metadata.tsv')

    mnist = input_data.read_data_sets('MNIST_data')
    images = tf.Variable(mnist.test.images, name='images')

    with open(metadata, 'w') as metadata_file:
        for row in mnist.test.labels:
            metadata_file.write('%d\n' % row)

    with tf.Session() as sess:
        saver = tf.train.Saver([images])

        sess.run(images.initializer)
        saver.save(sess, os.path.join(LOG_DIR, 'images.ckpt'))

        config = projector.ProjectorConfig()
        # One can add multiple embeddings.
        embedding = config.embeddings.add()
        embedding.tensor_name = images.name
        # Link this tensor to its metadata file (e.g. labels).
        embedding.metadata_path = metadata
        # Saves a config file that TensorBoard will read during startup.
        projector.visualize_embeddings(tf.summary.FileWriter(LOG_DIR), config)


if __name__ == "__main__":
    # create test-dataset for pca plot.
    # y is a vector and contains the cluster number of every data point. Predetermined by make_blobs
    data, y = create_test_data(3000, 100, 7, 3)

    # runs kmeans with dataset "data" and 7 clusters. Returns cluster data and respective cluster centers
    cluster_km, centers_km = cluster_kmeans(data, 7)

    # reduces dimensions to 3 dimensions using pca and plots clustered data
    pcaplot(data, cluster_km, centers_km)
