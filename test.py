import argparse  # for command line arguments
import json  # for reading the config file
# for filemanagement
from os import listdir
from os.path import isfile, isdir, join, exists, basename
# for time tracking
from time import time, sleep

import numpy as np
import tensorflow as tf
# all used metrics
from sklearn.metrics import davies_bouldin_score, silhouette_score, calinski_harabasz_score

import datamanager
from IDEC import IDEC
from jqmcvi import dunn_fast


def run_test(i, x, model_chkpt, seed):
    t_total_start = time()
    print(f"running test {i}")
    # load model and predict on data
    dims = [x.shape[-1], 300, 300, 1000, 10]
    n_clusters = 10

    t_modelload_start = time()
    idec = IDEC(dims, n_clusters=n_clusters)
    idec.initialize_model()
    idec.load_weights(model_chkpt)
    t_modelload_end = time()

    t_clustercenters_start = time()
    # cluster_centers = idec.model.get_layer(name="clustering").get_weights()
    t_clustercenters_end = time()

    t_encoder_start = time()
    encoder_pred, q = idec.encoder_and_cluster.predict(x)
    y_pred = q.argmax(1)  # this is the cluster prediction of IDEC
    t_encoder_end = time()
    t_pred_start = time()
    # y_pred = idec.predict(x)
    t_pred_end = time()

    # tf.summary calls write metrics to a file to view with tensorboard

    # count of unique clusters
    uniq, count = np.unique(y_pred, return_counts=True)
    unique_count = len(uniq)
    tf.summary.scalar("metrics/uniqcount", unique_count, step=i)

    # count per cluster
    tf.summary.histogram("metrics/counthist", y_pred, step=i)

    if unique_count > 1:
        # run all metrics and time them
        t_metricdb_start = time()
        metric_db = davies_bouldin_score(x, y_pred)
        metric_db_encoder = davies_bouldin_score(encoder_pred, y_pred)
        tf.summary.scalar("metrics/db", metric_db, step=i)
        tf.summary.scalar("metrics/db_encoder", metric_db, step=i)
        t_metricdb_end = time()
        t_metricdunn_start = time()
        metric_dunn = dunn_fast(x, y_pred)
        tf.summary.scalar("metrics/dunn", metric_dunn, step=i)
        t_metricdunn_end = time()
        t_metricshilouette_start = time()
        metric_shilouette = silhouette_score(x, y_pred, metric='euclidean', sample_size=None)
        tf.summary.scalar("metrics/shilouette", metric_shilouette, step=i)
        t_metricshilouette_end = time()
        t_chs_start = time()
        metric_chs = calinski_harabasz_score(x, y_pred)
        tf.summary.scalar("metrics/chs", metric_chs, step=i)
        t_chs_end = time()

    writer.flush()
    t_total_end = time()

    # print information
    if unique_count > 1:
        print(
            f"    unique: {unique_count}, db: {metric_db}, shilouette: {metric_shilouette}, dunn: {metric_dunn}, chs: {metric_chs}")
        print(
            f"    times: modelload: {t_modelload_end - t_modelload_start}\ centers: {t_clustercenters_end - t_clustercenters_start}\ encoder: {t_encoder_end - t_encoder_start}\ pred: {t_pred_end - t_pred_start}\ db: {t_metricdb_end - t_metricdb_start}\ dunn: {t_metricdunn_end - t_metricdunn_start}\ silhouette: {t_metricshilouette_end - t_metricshilouette_start}\ TOTAL: {t_total_end - t_total_start}")
    else:
        print(f"UNIQUE COUNT ONLY 1! (TOTAL time: {t_total_end - t_total_start}")


if __name__ == "__main__":

    checkpoint_dir = ""
    tensorboard_dir = ""

    parser = argparse.ArgumentParser(description='Testing model checkpoints.')

    parser.add_argument('-i', '--interval', type=float, default=10.000,
                        help="the time interval before the next check for new checkpoints (in seconds, float possible)")
    parser.add_argument('-l', '--last-checkpoint', type=int, help="if this checkpoint is encountered, then terminate")
    parser.add_argument('-t', '--terminate', action="store_true",
                        help="process all existing checkpoints and immediately terminate, overrides interval")
    parser.add_argument('-j', '--cpus', type=int, default=1, help="the number of cpu cores to use")

    parser.add_argument('-p', '--prefix', default="IDEC_model_",
                        help="text before the checkpoint number in the file name")
    parser.add_argument('-s', '--suffix', default=".h5", help="text after the checkpoint number in the file name")

    # parser.add_argument('-u','--uid', default="amazon_0.1_0.99_65536_5_300-300-1000-10_0.1", help="the unique id of the model to be tested")
    parser.add_argument('-u', '--uid', default="amazon_0.1_0.99_65536_10_300-300-1000-10_0.1",
                        help="the unique id of the model to be tested")
    parser.add_argument('-c', '--checkpoint-dir', default="./results/checkpoints/model_{}",
                        help="the directory containing the checkpoint files")
    parser.add_argument('-b', '--tensorboard-dir', default="./results/test/test_{}",
                        help="the directory to store tensorboard logs to")

    parser.add_argument('--seed', type=int, default=7357, help="the seed used for testing")
    parser.add_argument('--config', default="train_config.json", help="the configuration file")

    args = parser.parse_args()

    # each trained model has a unique ID from the hyperparameters - see train.py for more information
    checkpoint_dir = args.checkpoint_dir.format(args.uid)
    tensorboard_dir = args.tensorboard_dir.format(args.uid)
    print("checkpoint_dir: ", checkpoint_dir)
    print("tensorboard_dir: ", tensorboard_dir)

    #################################################################################################
    ## the following code finds all untested checkpoints, tests them and waits for new checkpoints ##
    #################################################################################################

    # a file for keeping track what was already tested
    tested_file = ".tested"

    checkpointfiles = {}
    if exists(join(tensorboard_dir, tested_file)):
        print("found .tested file")
        with open(join(tensorboard_dir, tested_file), "r") as tested:
            lines = [line.rstrip('\n') for line in tested]
            lines = [line[len(checkpoint_dir):] for line in lines if line.startswith(checkpoint_dir)]
            print("found {} already completed testruns".format(len(lines)))
            checkpointfiles = {"done-{}".format(i): basename(line) for i, line in enumerate(lines)}

    # test_writer = TensorboardLogger(join(tensorboard_dir))
    writer = tf.summary.create_file_writer(tensorboard_dir)
    with writer.as_default():

        # load the configuration from the config file
        with open(args.config) as conf:
            config = json.load(conf)
        print("config: ", config)
        x = datamanager.get_test_data(config['data'], config['embedded_data'])
        print("test data: ", x.shape)

        terminate = False
        while not terminate:
            terminate = args.terminate

            # find new checkpoints
            new_checkpointfiles = []
            if isdir(checkpoint_dir):
                new_checkpointfiles = [
                    f for f in listdir(checkpoint_dir)
                    if isfile(join(checkpoint_dir, f))
                    if f.startswith(args.prefix)
                    if f.endswith(args.suffix)
                    if f not in checkpointfiles.values()
                ]

            if new_checkpointfiles:
                new_checkpointfiles = {int(filename[len(args.prefix):len(filename) - len(args.suffix)]): filename for
                                       filename in new_checkpointfiles}

                # test all checkpoint files
                for i, filename in sorted(new_checkpointfiles.items()):
                    print("testing checkpoint #{}".format(i))
                    run_test(i, x, join(checkpoint_dir, filename), args.seed)

                    with open(join(tensorboard_dir, tested_file), "a") as tested:
                        tested.write("{}\n".format(join(checkpoint_dir, filename)))

                    if i == args.last_checkpoint:
                        print("Encountered last checkpoint {} - terminating".format(i))
                        exit()

                checkpointfiles.update(new_checkpointfiles)
            else:
                print("no new checkpoints")

            if args.terminate:
                print("Processed all checkpoint files - terminating")
                exit()

            print("waiting for new checkpoints...")
            # sleep interval
            sleep(args.interval)
