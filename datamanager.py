import contextlib  # for disabling polyglot warnings
import os  # for file management

# librarys for data operations (matrix and df)
import numpy as np
import pandas as pd
import unicodedata  # for filtering invalid characters
from nltk import word_tokenize
from nltk.corpus import stopwords
# libraries for stemming and tokenization
from nltk.stem.porter import *
from tqdm import tqdm  # progress bars

import doc2vec  # our doc2vec helper
from languagedetector import detect_language, UnknownLanguageException  # for detecting lannguage from text


# loading csv data from a given path and return it as a DataFrame
def load_data(data_path):
    # load data
    print("loading...")
    df = pd.read_csv(data_path)
    print(f"loading...done ({df.shape})")
    return df


# choose Amazon tweets; filter for initial user tweets only
def filter(df):
    df = df.drop(["created_at"], 1)
    company_filter = ['AmazonHelp']
    company_tweets = df[(df['inbound'] == False) & (df['author_id'].isin(company_filter))]

    user_tweets = df[df['tweet_id'].isin(company_tweets['in_response_to_tweet_id'])]
    initial_user_tweets = user_tweets[user_tweets['in_response_to_tweet_id'].isnull()]  # isna also works
    print("init user tweets count:", initial_user_tweets.shape)
    return initial_user_tweets


# text cleaning - unused in our training
def default_clean(text):
    '''
    Removes default bad characters
    '''
    # text = filter(lambda x: x in string.printable, text)
    bad_chars = set(
        ["@", "+", '/', "'", '"', '\\', '(', ')', '', '\\n', '', '?', '#', ',', '.', '[', ']', '%', '$', '&', ';', '!',
         ';', ':', "*", "_", "=", "}", "{"])
    for char in bad_chars:
        text = text.replace(char, " ")
    text = re.sub('\d+', "", text)
    return text


# stopwords and stemming - unused in our training
def stop_and_stem(text, stem=True, stemmer=PorterStemmer()):
    '''
    Removes stopwords and does stemming
    '''
    stoplist = stopwords.words('english')
    if stem:
        text_stemmed = [stemmer.stem(word) for word in word_tokenize(text) if word not in stoplist and len(word) > 3]
    else:
        text_stemmed = [word for word in word_tokenize(text) if word not in stoplist and len(word) > 3]
    text = ' '.join(text_stemmed)
    return text


# remove mentions, hashtags, URLs, non-english tweets
def clean(df):
    # if df should stay complete: instead of foreach, use 'apply' on the text column
    re_mention = re.compile(r'@[^\s]*')
    re_hashtag = re.compile(r'#[^\s]*')
    re_link = re.compile(r'(?:https?|www)[^\s]*')

    cleaned_texts = []

    # for all tweets:
    for text in tqdm(df['text']):
        # remove all mentions and hashtags
        matches = re_mention.findall(text)
        matches += re_hashtag.findall(text)
        for m in matches:
            text = text.replace(m, '')

        # remove all links
        link_matches = re_link.findall(text)
        for m in link_matches:
            # replace with _zzz_ to not impact language detector?!
            text = text.replace(m, '_zzz_')
        text = text.replace('amazon', '')
        text = text.strip()

        # this filters special control characters which the language detector cannot handle
        text = ''.join([l for l in text if unicodedata.category(l)[0] not in ('S', 'M', 'C')])

        try:
            # disables error messages momentarily
            with contextlib.redirect_stderr(None):
                # detects the languages
                langs = detect_language(text)
        except UnknownLanguageException:
            continue

        # list comprehension
        # this could be adjustable as a hyperparameter - languageconfidence
        langs = [l.code for l in langs if l.confidence > 95.]

        if not langs or 'en' not in langs:
            continue

        # replace with 'link' so doc2vec might infer some knowledge for seeing that there was some kind of link
        text = text.replace('_zzz_', 'link')

        # with cleaning and stemming the clustering gets way worse, since most information of the tweet is removed
        # text = default_clean(text)
        # text = stop_and_stem(text)

        cleaned_texts.append(text)
    return cleaned_texts


# for each tweet: perform vectorization and include single vectorized tweet into array "embedded" -> return entire array of vectorized tweets
def doc2vec_embedding(data):
    print("Doc2Vec on tweets...")

    embedded = []
    for tweet in tqdm(data):
        vec = doc2vec.vectorize(tweet)
        embedded.append(vec)

    return np.array(embedded)


# check first if vectorized has been done before. If so, load vectorized data. If not, vectorize.
def get_embedded_data(data_path, embedded_data_path=None):
    if embedded_data_path is not None and os.path.isfile(embedded_data_path):
        print("loading embeddings from file...")
        embeddings = np.load(embedded_data_path)  # load
        print("done")
    else:
        print("{} not found - preparing data".format(embedded_data_path))
        data = load_data(data_path)
        data = filter(data)
        print("filtered shape: ",len(data))
        data = clean(data)
        print("cleaned shape: ",len(data))
        embeddings = doc2vec_embedding(data)
        if embedded_data_path is not None:
            print("saving embeddings (shape: {})".format(embeddings.shape))
            np.save(embedded_data_path, embeddings)  # save
    return embeddings


# return cleaned tweets and embeddings for associating tweets with results
def get_cleaned_and_embedded_data(data_path, train_test_validiation_ind=1, seed=7357):
    data = load_data(data_path)
    data = filter(data)
    data = clean(data)

    if train_test_validiation_ind is not None:
        from sklearn.model_selection import train_test_split
        data_train, data_test = train_test_split(data, test_size=0.3, random_state=seed)
        data_test, data_validation = train_test_split(data_test, test_size=0.3, random_state=seed)

        data = [data_train, data_test, data_validation][train_test_validiation_ind]

    tweets = data
    embeddings = doc2vec_embedding(data)

    return tweets, embeddings


# split the data into train test and validation (default split: 70% train, 20% test, 10% validation)
def get_train_test_validation_data(data_path, embedded_data_path, seed=7357):
    # use splitting method provided by sklearn
    from sklearn.model_selection import train_test_split
    x = get_embedded_data(data_path, embedded_data_path)
    x_train, x_test = train_test_split(x, test_size=0.3, random_state=seed)
    x_test, x_validation = train_test_split(x_test, test_size=0.3, random_state=seed)
    return x_train, x_test, x_validation


# convenience method for training data
def get_train_data(data_path, embedded_data_path, seed=7357):
    train, _, _ = get_train_test_validation_data(data_path, embedded_data_path, seed)
    return train


# convenience method for test data
def get_test_data(data_path, embedded_data_path, seed=7357):
    _, test, _ = get_train_test_validation_data(data_path, embedded_data_path, seed)
    return test


# convenience method for validation data
def get_validation_data(data_path, embedded_data_path, seed=7357):
    _, _, validation = get_train_test_validation_data(data_path, embedded_data_path, seed)
    return validation

if __name__ == "__main__":
    train, test, validation = get_train_test_validation_data("twcs.csv", None)
    print(f"count: train: {train.shape}, test: {test.shape}, validation: {validation.shape}")
