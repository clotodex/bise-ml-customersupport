import os

import gensim

# loading the model
d2vec_path = os.path.join('vec_models', 'apnews_dbow', 'doc2vec.bin')
d2v_model = gensim.models.doc2vec.Doc2Vec.load(d2vec_path)
print("model loaded")


def vectorize(tweet):
    # splits tweets into single words, split by blanks
    parts = tweet.split(" ")

    # run d2v on words of one tweet in order to eliminate stop words
    vec = d2v_model.infer_vector(parts)

    return vec
