import json
import os
from time import time

import keras
from keras.optimizers import SGD

import datamanager
from IDEC import IDEC

# ignore memory warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


# convert hyperparameters into a unique id for filenames and such
def get_idec_uid(hyperparams):
    """
    This method converts the hyperparameters to a unique string to be used in file names etc
    """

    str_list = []
    for v in hyperparams.values():
        if type(v) is list:
            str_list.append('-'.join([str(x) for x in v]))
        else:
            str_list.append(str(v))
    return '_'.join(str_list)


def train_idec(config):
    x = datamanager.get_train_data(config['data'], config['embedded_data'])
    print("training data: ", x.shape)
    hyperparams = config['hyperparams']
    uid = get_idec_uid(hyperparams)
    uid = "amazon_" + uid
    print("hyperparams: ", hyperparams)
    print("UID: ", uid)

    # optimizer for training = backpropagations
    # lr = learning rate
    optimizer = SGD(lr=hyperparams['learning_rate'], momentum=hyperparams['momentum'])

    # prepare the IDEC model
    # dims = netzdefinition (-1 = letzes element)
    # batchsize = training mit so vielen daten gleichzeitig
    dims = hyperparams['dimensions']
    # first dimnsion is the input size
    dims = [x.shape[-1]] + dims
    n_clusters = hyperparams['n_clusters']
    batch_size = hyperparams['batchsize']  # actually has minimal impact on execution time
    idec = IDEC(dims=dims, n_clusters=n_clusters, batch_size=batch_size)

    # bastel des modell zusammen und initialisiere die neuronen zufällig
    gamma = hyperparams['gamma']
    idec.initialize_model(ae_weights=None, gamma=gamma, optimizer=optimizer)
    # plot_model(idec.model, to_file='idec_model.png', show_shapes=True)
    idec.model.summary()

    # other parameters
    t0 = time()
    tolerance = 0.001
    maxiter = 20000
    save_dir = config['checkpoint_dir'].format(uid)
    update_interval = 10

    # check if we already trained a model
    # get all existing checkpoint files
    start_iter = 0
    if os.path.exists(save_dir):
        checkpointfilename_prefix = "IDEC_model_"
        all_trained = [
            int(filename[len(checkpointfilename_prefix):].split('.')[0])
            for filename in os.listdir(save_dir) if
            os.path.isfile(os.path.join(save_dir, filename)) and filename.startswith(checkpointfilename_prefix)
        ]

        # select latest model
        if all_trained:
            latest = max(all_trained)
            start_iter = latest + 1
            print("latest_trained_model found '{}'".format(latest))

    # Create the TensorBoard callback, for logging training loss
    tensorboard = keras.callbacks.TensorBoard(
        log_dir=config['tensorboard_dir'].format(uid),
        histogram_freq=0,
        batch_size=batch_size,
        write_graph=True,
        write_grads=True
    )

    # fit the idec model
    y_pred = idec.fit_model(x, y=None, tol=tolerance, maxiter=maxiter,
                            update_interval=update_interval, save_dir=save_dir, tb_callback=tensorboard,
                            start_iter=start_iter)

    print('clustering time: ', (time() - t0))

    return idec.model


if __name__ == "__main__":
    # Parse command line arguments
    CONFIG_FILE = "train_config.json"

    # load the configuration from the config file
    with open(CONFIG_FILE) as conf:
        config = json.load(conf)
    print("config: ", config)

    # train the model
    train_idec(config)
