"""
Implementation for Improved Deep Embedded Clustering as described in paper:
        Xifeng Guo, Long Gao, Xinwang Liu, Jianping Yin. Improved Deep Embedded Clustering with Local Structure
        Preservation. IJCAI 2017.
Usage:
    Weights of Pretrained autoencoder for mnist are in './ae_weights/mnist_ae_weights.h5':
        python IDEC.py mnist --ae_weights ./ae_weights/mnist_ae_weights.h5
    for USPS and REUTERSIDF10K datasets
        python IDEC.py usps --update_interval 30 --ae_weights ./ae_weights/usps_ae_weights.h5
        python IDEC.py reutersidf10k --n_clusters 4 --update_interval 3 --ae_weights ./ae_weights/reutersidf10k_ae_weights.h5
Author:
    Xifeng Guo. 2017.4.30
"""

from time import time

from keras.models import Model
from keras.optimizers import SGD
from sklearn.cluster import KMeans

from DEC import ClusteringLayer, autoencoder


class IDEC(object):
    def __init__(self,
                 dims,
                 n_clusters=10,
                 alpha=1.0,
                 batch_size=256):

        super(IDEC, self).__init__()

        self.dims = dims
        self.input_dim = dims[0]
        self.n_stacks = len(self.dims) - 1

        self.n_clusters = n_clusters
        self.alpha = alpha
        self.batch_size = batch_size
        self.autoencoder = autoencoder(self.dims)

    def initialize_model(self, ae_weights=None, gamma=0.1, optimizer='adam'):
        if ae_weights is not None:
            self.autoencoder.load_weights(ae_weights)
            print('Pretrained AE weights are loaded successfully.')
        else:
            pass
            # print( 'ae_weights must be given. E.g.')
            # print( '    python IDEC.py mnist --ae_weights weights.h5')
            # exit()

        hidden_embedded = self.autoencoder.get_layer(name='encoder_%d' % (self.n_stacks - 1)).output
        self.encoder = Model(inputs=self.autoencoder.input, outputs=hidden_embedded)

        # prepare IDEC model
        clustering_layer = ClusteringLayer(self.n_clusters, name='clustering')(hidden_embedded)
        self.model = Model(inputs=self.autoencoder.input,
                           outputs=[clustering_layer, self.autoencoder.output])
        self.model.compile(loss={'clustering': 'kld', 'decoder_0': 'mse'},
                           loss_weights=[gamma, 1],
                           optimizer=optimizer)

        self.encoder_and_cluster = Model(inputs=self.autoencoder.input,
                                         outputs=[hidden_embedded, clustering_layer])

    def load_weights(self, weights_path):  # load weights of IDEC model
        self.model.load_weights(weights_path)

    def extract_feature(self, x):  # extract features from before clustering layer
        encoder = Model(self.model.input, self.model.get_layer('encoder_%d' % (self.n_stacks - 1)).output)
        return encoder.predict(x)

    def predict_clusters(self, x):  # predict cluster labels using the output of clustering layer
        q, _ = self.model.predict(x, verbose=0)
        return q.argmax(1)

    @staticmethod
    def target_distribution(q):  # target distribution P which enhances the discrimination of soft label Q
        weight = q ** 2 / q.sum(0)
        return (weight.T / weight.sum(1)).T

    def predict(self, x):
        return self.predict_clusters(x)

    def fit_model(self, x, y=None,
                  tol=1e-3,
                  update_interval=140,
                  maxiter=2e4,
                  save_dir='./results/idec',
                  tb_callback=None,
                  start_iter=0):

        if tb_callback is not None:
            tb_callback.set_model(self.model)

        # Transform train_on_batch return value
        # to dict expected by on_batch_end callback
        def named_logs(model, logs):
            result = {}
            for l in zip(model.metrics_names, logs):
                result[l[0]] = l[1]
            return result

        print('Update interval', update_interval)
        # save_interval = x.shape[0] / self.batch_size * 5  # 5 epochs
        save_interval = 10
        print('Save interval', save_interval)

        # initialize cluster centers using k-means
        print('Initializing cluster centers with k-means.')
        kmeans = KMeans(n_clusters=self.n_clusters, n_init=20)
        print("kmeans data: ", x[:200, ::].shape)
        y_pred = kmeans.fit_predict(self.encoder.predict(x[:200, ::]))
        y_pred_last = y_pred
        print("initializing last layer with initial fitted cluster centers")
        self.model.get_layer(name='clustering').set_weights([kmeans.cluster_centers_])
        print("initialized")

        # logging file
        import os
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        # logfile = file(save_dir + '/idec_log.csv', 'wb')
        # logwriter = csv.DictWriter(logfile, fieldnames=['iter', 'acc', 'nmi', 'ari', 'L', 'Lc', 'Lr'])
        # logwriter.writeheader()

        print(f"data shape: {x.shape}")

        if start_iter != 0:
            mod = start_iter % update_interval
            print("loading latest weights... (ep: {})".format(start_iter - 1))
            self.load_weights(os.path.join(save_dir, "IDEC_model_{}.h5".format(start_iter - 1)))
            print("done")
            if mod != 0:
                # TODO did not save those models, else uncomment and do model loading above afterwards
                # print("loading existing weights for dist update... (ep: {})".format(start_iter - mod -1))
                # self.load_weights(os.path.join(save_dir, "IDEC_model_{}.h5".format(start_iter - mod -1)))
                # print("done")
                q, _ = self.model.predict(x, verbose=0)
                p = self.target_distribution(q)  # update the auxiliary target distribution p

        # 3 types of loss: {'clustering': 'kld', 'decoder_0': 'mse'}
        loss = [0, 0, 0]
        index = 0
        ite_time_start = time()
        for ite in range(start_iter, int(maxiter)):
            ite_time_end = time()
            print(
                f"ite {ite} ({ite_time_end - ite_time_start}), Ep: {(ite * self.batch_size) // x.shape[0]} Rest: {(ite * self.batch_size) % x.shape[0]}")
            ite_time_start = time()
            if ite % update_interval == 0:
                print(f"Update {ite}")
                # to prevent instability, only update target distribution from time to time
                q, _ = self.model.predict(x, verbose=0)
                p = self.target_distribution(q)  # update the auxiliary target distribution p

                # Here would go "in training evaluation", we currently use separate test.py for parallelization reasons

            # train on batch
            if (index + 1) * self.batch_size > x.shape[0]:
                # last batch
                loss = self.model.train_on_batch(x=x[index * self.batch_size::],
                                                 y=[p[index * self.batch_size::], x[index * self.batch_size::]])
                tb_callback.on_epoch_end(ite, named_logs(self.model, loss))
                index = 0
            else:
                batchx = x[index * self.batch_size:(index + 1) * self.batch_size]
                batchy = [p[index * self.batch_size:(index + 1) * self.batch_size],
                          x[index * self.batch_size:(index + 1) * self.batch_size]]
                print(f"batch shapes: x={batchx.shape}, y={[b.shape for b in batchy]}")
                loss = self.model.train_on_batch(x=batchx,
                                                 y=batchy)
                tb_callback.on_epoch_end(ite, named_logs(self.model, loss))
                index += 1

            # save intermediate model
            if ite % save_interval == 0:
                # save IDEC model checkpoints
                print('saving model to:', save_dir + '/IDEC_model_' + str(ite) + '.h5')
                self.model.save_weights(save_dir + '/IDEC_model_' + str(ite) + '.h5')

            ite += 1

        tb_callback.on_train_end(None)

        # save the trained model
        print('saving model to:', save_dir + '/IDEC_model_final.h5')
        self.model.save_weights(save_dir + '/IDEC_model_final.h5')

        return y_pred


##################################################################
##  THE FOLLOWING IS JUST THE CODE FOR DIRECTLY EXECUTING IDEC  ##
##  FOR OUR EXECUTION LOOK AT "train.py"                        ##
##################################################################
if __name__ == "__main__":
    # setting the hyper parameters
    import argparse

    parser = argparse.ArgumentParser(description='train',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--n_clusters', default=10, type=int)
    parser.add_argument('--batch_size', default=256, type=int)
    parser.add_argument('--maxiter', default=2e4, type=int)
    parser.add_argument('--gamma', default=0.1, type=float,
                        help='coefficient of clustering loss')
    parser.add_argument('--update_interval', default=140, type=int)
    parser.add_argument('--tol', default=0.001, type=float)
    parser.add_argument('--ae_weights', default=None, help='This argument must be given')
    parser.add_argument('--save_dir', default='results/idec')
    args = parser.parse_args()
    print(args)

    import datamanager

    x = datamanager.get_embedded_data("twcs.csv", "twcs_embedded.npy")

    # optimizer for training = backpropagations
    # lr = learning rate
    optimizer = SGD(lr=0.1, momentum=0.99)

    # prepare the IDEC model
    # dims = netzdefinition (-1 = letzes element)
    # batchsize = training mit so vielen daten gleichzeitig
    idec = IDEC(dims=[x.shape[-1], 500, 500, 2000, 10], n_clusters=args.n_clusters, batch_size=args.batch_size)
    # bastel des modell zusammen und initialisiere die neuronen zufällig
    idec.initialize_model(ae_weights=args.ae_weights, gamma=args.gamma, optimizer=optimizer)
    # plot_model(idec.model, to_file='idec_model.png', show_shapes=True)
    idec.model.summary()

    # begin clustering, time not include pretraining part.
    t0 = time()
    y_pred = idec.fit_model(x, y=None, tol=args.tol, maxiter=args.maxiter,
                            update_interval=args.update_interval, save_dir=args.save_dir)
    # print('acc:', cluster_acc(y, y_pred))
    print('clustering time: ', (time() - t0))
