import csv

import numpy as np

import datamanager
from IDEC import IDEC
import time

##############################################################################
## HELPERSCRIPT FOR PRECTICTING A CLUSTER FOR TWEETS, USING A TRAINED MODEL ##
##############################################################################

if __name__ == "__main__":
    # load ALL data
    tweets, embeddings = datamanager.get_cleaned_and_embedded_data("twcs.csv", train_test_validiation_ind=None)

    print("predicting...")
    # parameters of trained model
    dims = [embeddings.shape[-1], 300, 300, 1000, 10]
    idec = IDEC(dims, n_clusters=50)
    idec.initialize_model()
    model_chkpt = "./results/checkpoints/model_amazon_0.1_0.99_65536_50_300-300-1000-10_0.1/IDEC_model_150.h5"
    idec.load_weights(model_chkpt)
    _start = time.time()
    y_pred = idec.predict(embeddings)
    _end = time.time()
    print(f"idec prediction took {_end - _start}")

    # some basic statistics so we know if it is worth even looking at the file
    u, counts = np.unique(y_pred, return_counts=True)

    print(f"Unique counts for {u}: {counts}")

    print("writing to file")
    with open('predicted_tweets.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        for cluster, tweet in zip(y_pred, tweets):
            writer.writerow([str(cluster), str(tweet)])
    print("done")
