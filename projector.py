import os

import tensorflow as tf

import datamanager
from IDEC import IDEC


#####################################################
## PROJECTOR IS A 3D VISUALIZATION TOOL FOR TWEETS ##
## because of some bugs you need TF version 1.15   ##
#####################################################

def visualize_witholdTF(tweets, embedded_data, clusters, tensorboard_dir):
    # import required tensorflow libraries
    import os
    import tensorflow as tf
    from tensorflow.contrib.tensorboard.plugins import projector

    # create directories
    try:
        os.mkdir(tensorboard_dir)
    except FileExistsError:
        pass

    # prepare path of metadata file
    LOG_DIR = tensorboard_dir
    metadata = os.path.join(LOG_DIR, 'metadata.tsv')

    # convert embedded data to tensorflow variable
    data_var = tf.Variable(embedded_data, name='data')

    # write metadata file, per tweet the cluster and the text for visualization
    with open(metadata, 'w') as metadata_file:
        metadata_file.write('Label\tTweet\n')
        for i, tweet in enumerate(tweets):
            metadata_file.write('{}\t{}\n'.format(clusters[i], tweet))

    # pre TF2.0 tensorflow session
    with tf.Session() as sess:
        saver = tf.train.Saver([data_var])

        sess.run(data_var.initializer)
        saver.save(sess, os.path.join(LOG_DIR, 'model.ckpt'))

        # configure projector
        config = projector.ProjectorConfig()
        # One can add multiple embeddings.
        embedding = config.embeddings.add()
        embedding.tensor_name = data_var.name
        # Link this tensor to its metadata file (e.g. labels).
        embedding.metadata_path = os.path.basename(metadata)
        # Saves a config file that TensorBoard will read during startup.
        projector.visualize_embeddings(tf.summary.FileWriter(LOG_DIR), config)


########################
## THIS DOES NOT WORK ##
########################
def visualize(tweets, embedded_data, clusters, tensorboard_dir):
    from tensorboard.plugins import projector
    print("creating metadata file...")
    try:
        os.mkdir(tensorboard_dir)
    except FileExistsError:
        pass
    meta_file = "metadata.tsv"
    with open(os.path.join(tensorboard_dir, meta_file), 'wb') as file_metadata:
        file_metadata.write('Label\tTweet\n'.encode('utf-8'))
        for i, tweet in enumerate(tweets):
            # temporary solution for https://github.com/tensorflow/tensorflow/issues/9094
            if tweet == '':
                print("Empty Line, should (prevent tensorboard bug)")
                file_metadata.write("{}\t{}".format('<Empty Line>', '<Empty Line>').encode('utf-8') + b'\n')
            else:
                file_metadata.write("{}\t{}".format(clusters[i], tweet).encode('utf-8') + b'\n')
    # create metadata file
    # metadata_path = os.path.join(tensorboard_dir, meta_file)
    # with open(metadata_path, 'w', encoding='utf8') as metadata_file:
    #    #metadata_file.write('Index\tLabel\tTweet\n')
    #    for i, tweet in enumerate(tweets):
    #        #metadata_file.write('{}\t{}\t{}\n'.format(i, clusters[i], tweet))
    #        metadata_file.write('{}\n'.format(tweet))

    print("creating TF variables and checkpoints...")
    embeddings = tf.Variable(embedded_data, name='embeddings')
    CHECKPOINT_FILE = tensorboard_dir + os.sep + 'model.ckpt'
    ckpt = tf.train.Checkpoint(embeddings=embeddings)
    ckpt.save(CHECKPOINT_FILE)

    print("configuring projector...")
    config = projector.ProjectorConfig()
    embedding = config.embeddings.add()
    embedding.tensor_name = embeddings.name
    # embedding.metadata_path = meta_file # metadata_path #

    print("writing data...")
    # writer = tf.summary.create_file_writer(tensorboard_dir)
    projector.visualize_embeddings(tensorboard_dir, config)
    print("done...")


if __name__ == "__main__":
    # load test data
    tweets, embeddings = datamanager.get_cleaned_and_embedded_data("twcs.csv", train_test_validiation_ind=1)

    print("predicting...")
    # set saved model's hyperparams for loading
    dims = [embeddings.shape[-1], 300, 300, 1000, 10]
    idec = IDEC(dims, n_clusters=50)
    idec.initialize_model()
    model_chkpt = "./results/checkpoints/model_amazon_0.1_0.99_65536_50_300-300-1000-10_0.1/IDEC_model_150.h5"
    idec.load_weights(model_chkpt)
    # predict the clustering of the tweets
    y_pred = idec.predict(embeddings)
    # encoded = idec.encoder.predict(embeddings)

    from distutils.version import StrictVersion

    if StrictVersion(tf.__version__) < StrictVersion('2.0.0'):
        print("using TF 1.0")
        visualize_witholdTF(tweets, embeddings, y_pred, "results/projector/")
    else:
        print("using TF 2.0")
        print("## WARNING: THIS WILL NOT WORK PROPERLY, INSTALL TF 1.15 AND RUN THIS SCRIPT AGAIN ##")
        visualize(tweets, encoded, y_pred, "results/projector/")
